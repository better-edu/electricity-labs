import pandas
import numpy as np
import matplotlib.pyplot as plt

csv_heating = '../data/lab-9_heating.csv'

table_heating = pandas.read_csv(csv_heating)

table_heating['T'] = table_heating['t']+273
table_heating['T^{-1}'] = 1/table_heating['T']
table_heating['lnR_sc'] = np.log(table_heating['R_sc'])

table_heating.to_csv('../data/lab-9_heating.gen.csv',index=False)

# plotting addiction R(t) conductors
# set grid
plt.grid()

# trending line in plot
x = np.polyfit(table_heating['t'],table_heating['R_c'],1)
p = np.poly1d(x)

plt.plot(table_heating['t'],p(table_heating['t']),"r--")

# find equeation in plot
equ = "y=%.6fx+%.6f"%(x[0],x[1])

plt.plot(table_heating['t'],table_heating['R_c'], label=equ)

plt.title("Addiction R(t)")
plt.xlabel("t,C")
plt.ylabel("R, Om")
plt.legend(fontsize=14)

# save a jpeg picture
plt.savefig('../static/plot-conductors.jpg')

# clear field with next plot
plt.clf()

# set grid
plt.grid()

# trending line in plot
y = np.polyfit(table_heating['T'],table_heating['R_sc'],2)
p = np.poly1d(y)

plt.plot(table_heating['T'],p(table_heating['T']),"r--")

plt.plot(table_heating['T'],table_heating['R_sc'])

plt.title("Addiction R(t)")
plt.xlabel("T,K")
plt.ylabel("R, kOm")

plt.savefig('../static/plot-semiconductors.jpg')

plt.clf()

plt.grid()

z = np.polyfit(table_heating['T^{-1}'],table_heating['lnR_sc'],1)
p = np.poly1d(z)

plt.plot(table_heating['T^{-1}'],p(table_heating['T^{-1}']),"r--")

equ = "y=%.6fx%.6f"%(z[0],z[1])

plt.plot(table_heating['T^{-1}'],table_heating['lnR_sc'],label=equ)

plt.title("Addiction lnR(t)")
plt.xlabel("1/T,K")
plt.ylabel("lnR")
plt.legend(fontsize=14)

plt.savefig('../static/plot-semiconductors_log.jpg')
