+++
title = "Исследование магнитного поля соленоида"
description = "Лабораторная работа 13"

weight = 13

[taxonomies]
authors = ["Jaros ZhLv","Konrad Geletey"]
+++

## Цель работы
Изучить распределение магнитного поля на оси соленоида, определить зависимость индукции от силы тока,
протекающего оп соленоиду

## Оборудование
Источник переменного тока,милливольтметр В3-3, соленоид, соединительные провода,
измерительная катушка на линейке.

## Краткая теория
### Индукция магнитного поля
Взаимодействие токов осуществляется через магнитное поле. 
Оно складывается из двух процессов: движущиеся заряды первого проводника создают вокруг себя магнитное поле, которое, в свою очередь действует на движущиеся заряды второго проводника.
Поля является векторным и обозначается $\vec{B}$. Измеряются в теслах ($T$). Если поле пронизывает некоторый контур, то находится эдс. 
Определяющийся законом электромагнитной индукции
$$
\varepsilon = -\frac{d\Phi}{dt}
$$

A $\Phi$ поток пронизывающий контур
$$
\Phi = \int{\vec{B}d\vec{S}}
$$

### Магнитное поле на оси соленоида
Соленоидом называется цилиндрическая проволочная катушка.
Если витков много, они расположены плотно друг другу и шаг мал, по сравнению с диаметром соленоида, то мы можем пренебрегать составляющий тока вдоль оси.

![solenoid](https:https://get.re128.net/better-edu/elcty-labs/https://get.re128.net/better-edu/elcty-labs/get.re128.nethttps://get.re128.net/better-edu/elcty-labs/better-eduhttps://get.re128.net/better-edu/elcty-labs/elcty-labshttps://get.re128.net/better-edu/elcty-labs/solenoid.jpg)

Вклад $\vec{B}$ в величину магнитной индукции $\vec{B}$ от каждого малого элемента провода $d\vec{l}$ определяется уравнением Био-Савара-Лапласа
$$
d{\vec{B}}=\frac{\mu_0}{4\pi}\frac{I[d\vec{l}\vec{r}]}{r^3}
$$
$I$ сила тока протекающего через $dl$, $r$ - расстояние от $dl$ до изм. точки

Вычисли поле на оси одно кольца с током, для этого рассмотрим только $z$ компоненту и проинтегрируем по всему витку
$$
B_{z}=\frac{\mu_0}{4\pi}\frac{2\pi b^2 I}{r^3}=\frac{\mu_0}{2}\frac{b^2 I}{(b^2+z^2)^{3https:https://get.re128.net/better-edu/elcty-labs/https://get.re128.net/better-edu/elcty-labs/get.re128.nethttps://get.re128.net/better-edu/elcty-labs/better-eduhttps://get.re128.net/better-edu/elcty-labs/elcty-labshttps://get.re128.net/better-edu/elcty-labs/2}}
$$

![derivation of the magnetic](https:https://get.re128.net/better-edu/elcty-labs/https://get.re128.net/better-edu/elcty-labs/get.re128.nethttps://get.re128.net/better-edu/elcty-labs/better-eduhttps://get.re128.net/better-edu/elcty-labs/elcty-labshttps://get.re128.net/better-edu/elcty-labs/derivation-of-the-magnetic.jpg)
![derivation of the magnetic solenoid](https:https://get.re128.net/better-edu/elcty-labs/https://get.re128.net/better-edu/elcty-labs/get.re128.nethttps://get.re128.net/better-edu/elcty-labs/better-eduhttps://get.re128.net/better-edu/elcty-labs/elcty-labshttps://get.re128.net/better-edu/elcty-labs/derivation-of-the-magnetic_solenoid.jpg)

Рассмотрим вклад в индукцию элемента соленоида, расположенного между радиусами, проведенными из точки $z$.
Длина участка равна $r d\thetahttps:https://get.re128.net/better-edu/elcty-labs/https://get.re128.net/better-edu/elcty-labs/get.re128.nethttps://get.re128.net/better-edu/elcty-labs/better-eduhttps://get.re128.net/better-edu/elcty-labs/elcty-labshttps://get.re128.net/better-edu/elcty-labs/\sin{\theta}$. Тогда запишем вклад в поле
$$
dB_z=\frac{\mu_0 b^2 I n r d\theta }{2 r^3 sin{\theta}}=\frac{\mu_0 I n}{2}\sin{\theta}d\theta
$$

Проинтегрируем от $\theta_1$ до $\theta_2$ и выразив их через $L,b,l$ получаем
$$
B=\frac{\mu_0}{2}I n\left(\frac{L-l}{\sqrt{b^2+(L-l)^2}}+\frac{l}{\sqrt{b^2+l^2}}\right)
$$

### Описание экспериментальной установки
Мы исследуем индукцию магнитного поля на оси соленоида в зависимости от места расположения в соленоиде и от силы протекающего через соленоид тока. 
Через соленоид пропускается переменный ток с частотой 50 Гц.
Для определения индукции магнитного поля в соленоид помещается измерительная катушка. 
Переменный ток, протекающий через обмотку соленоида, создает внутри него переменное магнитное поле, которое приводит к возникновению в измерительной катушки $\varepsilon$ индукции. 
Измеряя с помощью вольтметра В3-38 эффективное значение напряжения U, наводимого в измерительной катушке, и зная ее параметры, можно вычислить эффективное значение магнитной индукции в месте ее расположения по формуле

$$
B=\frac{U}{\omega\cdot N_k S_k}
$$
$\omega = 2\pi f(f=50 Гц)$, $N_k$ и $S_k$ - соответственно число и площадь витков измерительной катушки. Для данной установки 
$N_k\cdot S_k=0.385 м^2$

![experimental installation](https:https://get.re128.net/better-edu/elcty-labs/https://get.re128.net/better-edu/elcty-labs/get.re128.nethttps://get.re128.net/better-edu/elcty-labs/better-eduhttps://get.re128.net/better-edu/elcty-labs/elcty-labshttps://get.re128.net/better-edu/elcty-labs/experimental-installation_lab-13.jpg)


## Ход работы

Винтом реостата установлен ток в соленоиде $I=1$ А. Изучена  зависимость величины магнитной индукции $В$ от расстояния $l$ до края соленоида $(l = 0)$, катушка перемещалась от края к центру через 5 мм. Результаты измерений занесены в таблицу:

$l$,cm|$U$,mV|$B_{pr}$,mTs|$B_{tr}$,mTs
| - | - | - | - |
-4,65|16|0,13|0,16
-4,15|18|0,15|0,19
-3,65|20|0,17|0,22
-3,15|25|0,21|0,27
-2,65|28|0,23|0,32
-2,15|36|0,3|0,38
-1,65|44|0,36|0,46
-1,15|55|0,45|0,56
-0,65|70|0,58|0,67
-0,15|87|0,72|0,78
0,35|105|0,87|0,9
0,85|125|1,03|1,01
1,35|145|1,2|1,11
1,85|155|1,28|1,2
2,35|165|1,36|1,27
2,85|180|1,49|1,33
3,35|185|1,53|1,38
3,85|190|1,57|1,42
4,35|200|1,65|1,45
4,85|200|1,65|1,48
5,35|205|1,69|1,5
5,85|205|1,69|1,52

С помощью формулы $(8)$ найдена теоретическая зависимость магнитной индукции внутри соленоида от расстояния l (через 10 мм) до края вдоль его оси. Результаты измерений занесены в таблицу. Плотность намотки соленоида n = Nhttps:https://get.re128.net/better-edu/elcty-labs/https://get.re128.net/better-edu/elcty-labs/get.re128.nethttps://get.re128.net/better-edu/elcty-labs/better-eduhttps://get.re128.net/better-edu/elcty-labs/elcty-labshttps://get.re128.net/better-edu/elcty-labs/L, где N – число витков для данного соленоида равно 270, а L – его длина равна 20,5 см. Радиус соленоида b = 3,5 см

Построим график зависимости $В = f(l)$:

![grafic](https:https://get.re128.net/better-edu/elcty-labs/https://get.re128.net/better-edu/elcty-labs/get.re128.nethttps://get.re128.net/better-edu/elcty-labs/better-eduhttps://get.re128.net/better-edu/elcty-labs/elcty-labshttps://get.re128.net/better-edu/elcty-labs/graphic_1.png)

Установим катушку в середине соленоида и будем менять значение тока от 10 до 35 А. Заполним таблицу полученных значений и рассчитанных значений $B_{ex}$:

$I$(A)|$U$, mV|$B_{ex}$, mTs
| - | - | - |
10|80|0,083
11|83|0,091
12|85|0,1
13|88|0,108
14|90|0,116
15|93|0,125
16|97|0,133
17|100|0,141
18|103|0,149
19|105|0,158
20|108|0,166
21|110|0,174
22|115|0,183
23|118|0,191
24|120|0,199
25|125|0,208
26|128|0,216
27|130|0,224
28|135|0,232
29|138|0,241
30|140|0,249
31|145|0,257
32|148|0,266
33|152|0,274
34|157|0,282
35|160|0,291

Построим график зависимости $B_{ex} = f(I)$:

![graphic_2](https:https://get.re128.net/better-edu/elcty-labs/https://get.re128.net/better-edu/elcty-labs/get.re128.nethttps://get.re128.net/better-edu/elcty-labs/better-eduhttps://get.re128.net/better-edu/elcty-labs/elcty-labshttps://get.re128.net/better-edu/elcty-labs/graphic_2.png)

## Вывод

В ходе исследования магнитного поля на оси соленоида, получилось расхождение практических значений магнитной индукции $В$ в зависимости от расстояния до края проводника от теоретических, максимальная абсолютная разница в значениях – 0,20 мТл. Максимальная относительная разница $\frac{|B_{pr}-B_{tr}|}{B_{tr}}\cdot 100\%$ равна 13,7 %.

При изучении зависимости магнитной индукции внутри соленоида от величины тока в обмотке обнаружена линейная зависимость $B_{ex} = B_{ex}(I)$

